EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:pspice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 54ED4FDE
P 7000 3700
F 0 "R1" V 7080 3700 50  0000 C CNN
F 1 "10" V 7007 3701 50  0000 C CNN
F 2 "" V 6930 3700 30  0000 C CNN
F 3 "" H 7000 3700 30  0000 C CNN
	1    7000 3700
	-1   0    0    -1  
$EndComp
$Comp
L LD1117S12CTR X1
U 1 1 54ED508D
P 6000 3150
F 0 "X1" H 6000 3400 40  0000 C CNN
F 1 "LD1117_33" H 6000 3350 40  0000 C CNN
F 2 "SOT-223" H 6000 3250 40  0000 C CNN
F 3 "" H 6000 3150 60  0000 C CNN
	1    6000 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 54ED5104
P 6000 4400
F 0 "#PWR01" H 6000 4150 60  0001 C CNN
F 1 "GND" H 6000 4250 60  0000 C CNN
F 2 "" H 6000 4400 60  0000 C CNN
F 3 "" H 6000 4400 60  0000 C CNN
	1    6000 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3400 6000 4400
Wire Wire Line
	7000 4300 7000 3950
Wire Wire Line
	5100 4300 7000 4300
Connection ~ 6000 4300
Wire Wire Line
	6400 3100 7000 3100
Wire Wire Line
	7000 3100 7000 3450
Wire Wire Line
	5100 3100 5600 3100
$Comp
L CONN_01X02 V1
U 1 1 54ED50E5
P 3800 3700
F 0 "V1" H 3800 3850 50  0000 C CNN
F 1 "PULSE(-2.5V 2.5V 5uS 5uS 5uS 15uS 40uS)" V 4000 3700 50  0000 C CNN
F 2 "" H 3800 3700 60  0000 C CNN
F 3 "" H 3800 3700 60  0000 C CNN
	1    3800 3700
	-1   0    0    -1  
$EndComp
Text GLabel 5300 3100 1    60   Input ~ 0
IN
Text GLabel 7000 3100 2    60   Input ~ 0
OUT
Text Notes 3800 5250 0    60   ~ 0
-pspice\n* \n.include LD1117.prm\n.include ss14.txt
Text Notes 5100 5450 0    60   ~ 0
+pspice\n* \n.control\n  options nopage\n  *print v(in) v(out) > project.sim\n  tran 0.25uS 0.5mS 0mS\n  set gnuplot_terminal=png\n  gnuplot project v(ac0) v(ac1) v(in) v(out)\n.endc\n
$Comp
L C C1
U 1 1 54ED5E49
P 5500 3400
F 0 "C1" H 5550 3500 50  0000 L CNN
F 1 "100u" H 5550 3300 50  0000 L CNN
F 2 "" H 5538 3250 30  0000 C CNN
F 3 "" H 5500 3400 60  0000 C CNN
	1    5500 3400
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 54ED5EA9
P 6500 3700
F 0 "C2" H 6550 3800 50  0000 L CNN
F 1 "100u" H 6550 3600 50  0000 L CNN
F 2 "" H 6538 3550 30  0000 C CNN
F 3 "" H 6500 3700 60  0000 C CNN
	1    6500 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6500 3900 6500 4300
Connection ~ 6500 4300
Wire Wire Line
	6500 3500 6500 3100
Connection ~ 6500 3100
Wire Wire Line
	5500 3200 5500 3100
Connection ~ 5500 3100
$Comp
L DIODESCH X2
U 1 1 54ED67CD
P 4900 3100
F 0 "X2" H 4900 3200 50  0000 C CNN
F 1 "SS14" H 4900 3000 50  0000 C CNN
F 2 "" H 4900 3100 60  0000 C CNN
F 3 "" H 4900 3100 60  0000 C CNN
	1    4900 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3100 4700 3100
Text GLabel 4600 3100 0    60   Input ~ 0
AC1
Wire Wire Line
	4000 3750 4100 3750
Wire Wire Line
	4100 3750 4100 3850
Wire Wire Line
	4000 3650 4100 3650
Wire Wire Line
	4100 3650 4100 3550
$Comp
L C C3
U 1 1 54EDB66A
P 6750 3700
F 0 "C3" H 6800 3800 50  0000 L CNN
F 1 "100n" H 6800 3600 50  0000 L CNN
F 2 "" H 6788 3550 30  0000 C CNN
F 3 "" H 6750 3700 60  0000 C CNN
	1    6750 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6750 3900 6750 4300
Connection ~ 6750 4300
Wire Wire Line
	6750 3500 6750 3100
Connection ~ 6750 3100
Text GLabel 4100 3550 1    60   Input ~ 0
AC1
Text GLabel 4100 3850 3    60   Input ~ 0
AC0
$Comp
L C C4
U 1 1 54FFD3CF
P 5500 4000
F 0 "C4" H 5550 4100 50  0000 L CNN
F 1 "100u" H 5550 3900 50  0000 L CNN
F 2 "" H 5538 3850 30  0000 C CNN
F 3 "" H 5500 4000 60  0000 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3600 5500 3800
Text GLabel 5400 3700 0    60   Input ~ 0
AC0
Wire Wire Line
	5400 3700 5500 3700
Connection ~ 5500 3700
Wire Wire Line
	5500 4200 5500 4300
$Comp
L DIODESCH X3
U 1 1 54FFD548
P 4900 4300
F 0 "X3" H 4900 4400 50  0000 C CNN
F 1 "SS14" H 4900 4200 50  0000 C CNN
F 2 "" H 4900 4300 60  0000 C CNN
F 3 "" H 4900 4300 60  0000 C CNN
	1    4900 4300
	-1   0    0    -1  
$EndComp
Text GLabel 4600 4300 0    60   Input ~ 0
AC1
Wire Wire Line
	4600 4300 4700 4300
Connection ~ 5500 4300
$EndSCHEMATC
